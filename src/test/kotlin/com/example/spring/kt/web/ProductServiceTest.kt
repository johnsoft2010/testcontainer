package com.example.spring.kt.web

import io.zonky.test.db.AutoConfigureEmbeddedDatabase
import org.aspectj.lang.annotation.Before
import org.assertj.core.api.Assertions
import org.flywaydb.test.annotation.FlywayTest
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpStatus
import java.util.*

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureEmbeddedDatabase(provider = AutoConfigureEmbeddedDatabase.DatabaseProvider.DOCKER)
class ProductServiceTest(@Autowired val productRepository: ProductRepository) {

    @BeforeEach
    fun before() {
        productRepository.save(Product(name = "test", id = 1))
        //val all = productRepository.findAll()
    }

    @Test
    fun findIncorrectProduct() {
        val product = productRepository.findById(1)
        Assertions.assertThat(product.isPresent).isTrue()

    }


}
