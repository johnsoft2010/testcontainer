package com.example.spring.kt.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

public class Test {

    public static void main(String[] args) {

        var list =
        List.of(
                Map.of("a", List.of("d", "e")),
                Map.of("b", List.of("ds", "g")),
                Map.of("a", List.of("s", "f")),
                Map.of("d", List.of("fdfgf", "fgfgdfg")),
                Map.of("b", List.of("1", "2"))
        );

        List<Map<String, Integer>> list2 =
                List.of(
                        Map.of("a", 1),
                        Map.of("b", 2),
                        Map.of("a", 3)
                );

        var d =
        list.stream()
                .flatMap(m -> m.entrySet().stream())
                .collect(
                groupingBy(e -> e.getKey(),
                        collectingAndThen(toList(),
                                e -> e.stream()
                                        .flatMap(r -> r.getValue().stream())
                                        .collect(toList()))
                        //mapping( e -> e.getValue().stream().collect(toList()), toList() )
                        //reducing("", Map.Entry::getValue, (o, o2) -> o.toString() + o2.toString())
                    )
                );
        System.out.println(d);

/*        var d1 =
                list.stream()
                        .flatMap(m -> m.entrySet().stream())
                        .collect(
                                groupingBy(
                                        Map.Entry::getKey,
                                        collectingAndThen(new ArrayList<String>(),
                                                r -> r.toString())
                                )
                        );*/

    }
}
