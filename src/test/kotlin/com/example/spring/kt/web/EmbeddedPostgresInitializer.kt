package com.example.spring.kt.web

import org.springframework.context.ApplicationContextInitializer
import org.springframework.context.support.GenericApplicationContext

class EmbeddedPostgresInitializer : ApplicationContextInitializer<GenericApplicationContext> {

    override fun initialize(applicationContext: GenericApplicationContext) {
//        val postgres = EmbeddedPostgres()
//        val url = postgres.start()
//        val values = TestPropertyValues.of(
//                "spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.PostgreSQLDialect",
//                "spring.test.database.replace=none",
//                "spring.datasource.url=$url",
//                "spring.datasource.driver-class-name=org.postgresql.Driver",
//                "spring.jpa.hibernate.ddl-auto=create")
//        values.applyTo(applicationContext)
//        applicationContext.registerBean(EmbeddedPostgres::class.java, Supplier { postgres },
//                BeanDefinitionCustomizer { beanDefinition -> beanDefinition.destroyMethodName = "stop" })
    }
}