package com.example.spring.kt.web

import org.junit.jupiter.api.Test
import java.util.function.Function
import java.util.stream.Collectors
import java.util.stream.Collectors.*

class SimpleTest {

    @Test
    fun test() {
        val fruits = listOf("apple", "acotpri", "banana", "blueberry", "cherry", "coconut")

// collect only even length Strings
        val evenFruits = fruits.groupingBy { it.first() }
        evenFruits.foldTo(mutableMapOf(), emptyList<String>()) { acc, e -> if (e.length % 2 == 0) acc + e else acc }

        println(evenFruits) // {a=[], b=[banana], c=[cherry]}

        val list = listOf(
                mapOf("a" to listOf("a","b"), "b" to listOf("a","b")),
                mapOf("b" to listOf("c","d")),
                mapOf("a" to listOf("r","v"))
        )
        val res = list.flatMap { m -> m.entries }
                .groupingBy { it.key }
                .foldTo(mutableMapOf(), emptyList<String>()) { acc, e -> acc + e.value }

        println(res)


        val d = list.stream()
                .flatMap { m -> m.entries.stream() }
                .collect(
                        groupingBy(Function { e: Map.Entry<String?, List<String?>> -> e.key },
                                collectingAndThen(toList(),
                                        Function { e: List<Map.Entry<String?, List<String?>>> ->
                                            e.stream()
                                                    .flatMap { r -> r.value.stream() }
                                                    .collect(toList())
                                        })
                        )
                )
        println(d)

        assert(true)
    }
}