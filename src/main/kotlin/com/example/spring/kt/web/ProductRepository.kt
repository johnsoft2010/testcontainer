package com.example.spring.kt.web

import org.springframework.data.repository.CrudRepository

interface ProductRepository : CrudRepository<Product, Long> {
}