package com.example.spring.kt.web

import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException

@RestController
@RequestMapping("products")
class ProductsController(private var productRepository: ProductRepository) {

    @GetMapping("{id}")
    @ResponseStatus(HttpStatus.FOUND)
    fun find(@PathVariable id: Long, @RequestParam id2: Long)  {
        println(id2)
        productRepository.findById(id).orElseThrow { ResponseStatusException(HttpStatus.NOT_FOUND) }
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun create(@RequestBody product: Product) = productRepository.save(product)

    @PutMapping("{id}")
    fun update(@PathVariable id: Long, @RequestBody product: Product) = productRepository.save(product.copy(id=id))
}