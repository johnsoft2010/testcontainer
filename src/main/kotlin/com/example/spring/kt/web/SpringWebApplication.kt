package com.example.spring.kt.web

import org.springframework.boot.Banner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpringWebApplication

fun main(args: Array<String>) {
    runApplication<SpringWebApplication>(*args) {
        setBannerMode(Banner.Mode.OFF)
        setLogStartupInfo(false)

    }
}
