package com.example.spring.kt.web

import io.zonky.test.db.AutoConfigureEmbeddedDatabase
import org.aspectj.lang.annotation.Before
import org.assertj.core.api.Assertions
import org.flywaydb.test.annotation.FlywayTest
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpStatus

@Tag("integration")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureEmbeddedDatabase(provider = AutoConfigureEmbeddedDatabase.DatabaseProvider.DOCKER)
//https://www.baeldung.com/spring-security-integration-tests
//https://www.baeldung.com/parameterized-tests-junit-5
//https://stackoverflow.com/questions/48431381/java-8-groupingby-into-map-that-contains-a-list
class SpringWebApplicationIT(@Autowired val restTemplate: TestRestTemplate,
                             @Autowired val productRepository: ProductRepository) {

    @BeforeEach
    fun before() {
        productRepository.save(Product(name = "test", id = 1))
        //val all = productRepository.findAll()
    }

    @Test
    fun findIncorrectProduct() {
        val error = restTemplate.getForEntity("/products", Product::class.java)
        Assertions.assertThat(error.statusCode).isEqualTo(HttpStatus.METHOD_NOT_ALLOWED)
    }

    @Test
    fun findProductNotFound() {
        val entity = restTemplate.getForEntity("/products/{id}", Product::class.java, 1)
        Assertions.assertThat(entity.statusCode).isEqualTo(HttpStatus.FOUND)
    }

    @Test
    fun findProduct() {
        val entity = restTemplate.getForEntity("/{id}", Product::class.java, 0)
        Assertions.assertThat(entity.statusCode).isEqualTo(HttpStatus.NOT_FOUND)
    }


}
